# This entire file is to utilize polling to retreive data
import json
import requests

from .models import ConferenceVO

# Function designed to poll the Conference model from the Monolith
def get_conferences():
    # Assigns the url for the Conferences url that uses the list conference view
    url = "http://monolith:8000/api/conferences/"
    # Requests will automatically decode content from the server
    response = requests.get(url)
    # Decodes the json retreived from the response variable
    content = json.loads(response.content)
    # Loops through the decoded data to put the information into the ConferenceVO
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
