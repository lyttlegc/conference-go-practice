from common.json import ModelEncoder
from .models import Attendee

# Get rid of the events imprt because it is in the monolith
# from events.encoders import ConferenceListEncoder
# Replace with:
from .models import ConferenceVO

# Creates the encoder for our new ConferenceVO
# Replace the old conference list encoder with this one, anywhere found
class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder,
        # "conference": ConferenceListEncoder(),
    }
