import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# API that uses the requests module to get third party data from Pexels
def get_photo(city, state):
    # Creates a dictionary for the necessary headers required for Pexels API
    headers = {"Authorization": PEXELS_API_KEY}
    # Query for searching for photos, required by Pexels
    query = f"{city} {state}"
    # Creates the url for the function to search when pulling data
    url = f"https://api.pexels.com/v1/search?query={query}"
    # Makes the request from Pexels via requests with the url and headers
    response = requests.get(
        url,
        headers=headers,
    )
    # Returns the link for a picture of the data requested
    # response.json decodes the json formatted string
    return {"picture_url": response.json()["photos"][0]["src"]["original"]}


# API that uses the requests module to get third party data from Open Weather
def get_weather_data(city, state):
    query = f"{city},{state},US"
    appid = OPEN_WEATHER_API_KEY
    geo_url = (
        f"http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={appid}"
    )
    geo_response = requests.get(geo_url)
    geo_data = geo_response.json()
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]

    if lat is None or lon is None:
        return None
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={appid}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()
    return {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["main"],
    }
