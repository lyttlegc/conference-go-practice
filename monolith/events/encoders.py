from common.json import ModelEncoder
from .models import Conference, Location

# Creates an encoder for api_list_location
class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    # Gets extra data, state's abbreviation, and returns it to a dictionary
    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


# Creates an encoder for api_show_conference
class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    # Ties the LocationList Encoder to the "location" property so it can
    # pull the information out of the Location model
    encoders = {
        "location": LocationListEncoder(),
    }


# Creates an encoder for api_list_conference
class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]
