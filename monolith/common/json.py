from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        # Check if the object is an instance of the datetime module
        if isinstance(o, datetime):
            # Returns the object as an ISO formatted string
            return o.isoformat()
        else:
            # Call the default serialization behavior
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        # Check if the object is an instance of a QuerySet
        if isinstance(o, QuerySet):
            # Returns the object as a list
            return list(o)
        else:
            # Call the default serialization behavior
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        # Check if the object is an instance of a specific class
        if isinstance(o, self.model):
            d = {}  # Create an empty dictionary to store serialized properties
            # Checks if the object has the attribute "get_api_url"
            if hasattr(o, "get_api_url"):
                # If so, adds the key and value to the dictionary
                d["href"] = o.get_api_url()
            for prop in self.properties:
                # Get the value of the property from the object
                value = getattr(o, prop)
                # Check if 'property' exists as a key in the 'self.encoders' dictionary
                if prop in self.encoders:
                    # Retrieve the corresponding encoder object from the dictionary
                    encoder = self.encoders[prop]
                    # Call the 'default' method of the encoder object and update the 'value'
                    value = encoder.default(value)
                # Add the property name and value to the dictionary
                d[prop] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            # Call the default serialization behavior
            return super().default(o)

    def get_extra_data(self, o):
        return {}
